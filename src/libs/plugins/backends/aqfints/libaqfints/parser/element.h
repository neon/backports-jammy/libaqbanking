/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "element.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AQFINTS_ELEMENT_ELEMENT_H
#define AQFINTS_ELEMENT_ELEMENT_H


#ifdef __cplusplus
extern "C" {
#endif

/** @page P_AQFINTS_ELEMENT Structure AQFINTS_ELEMENT
<p>This page describes the properties of AQFINTS_ELEMENT.</p>



<h1>AQFINTS_ELEMENT</h1>



@anchor AQFINTS_ELEMENT_flags
<h2>flags</h2>

<p>Set this property with @ref AQFINTS_Element_SetFlags(), get it with @ref AQFINTS_Element_GetFlags().</p>


@anchor AQFINTS_ELEMENT_elementType
<h2>elementType</h2>

<p>Set this property with @ref AQFINTS_Element_SetElementType(), get it with @ref AQFINTS_Element_GetElementType().</p>


@anchor AQFINTS_ELEMENT_id
<h2>id</h2>

<p>Set this property with @ref AQFINTS_Element_SetId(), get it with @ref AQFINTS_Element_GetId().</p>


@anchor AQFINTS_ELEMENT_name
<h2>name</h2>

<p>Set this property with @ref AQFINTS_Element_SetName(), get it with @ref AQFINTS_Element_GetName().</p>


@anchor AQFINTS_ELEMENT_version
<h2>version</h2>

<p>Set this property with @ref AQFINTS_Element_SetVersion(), get it with @ref AQFINTS_Element_GetVersion().</p>


@anchor AQFINTS_ELEMENT_type
<h2>type</h2>

<p>Set this property with @ref AQFINTS_Element_SetType(), get it with @ref AQFINTS_Element_GetType().</p>


@anchor AQFINTS_ELEMENT_minNum
<h2>minNum</h2>

<p>Set this property with @ref AQFINTS_Element_SetMinNum(), get it with @ref AQFINTS_Element_GetMinNum().</p>


@anchor AQFINTS_ELEMENT_maxNum
<h2>maxNum</h2>

<p>Set this property with @ref AQFINTS_Element_SetMaxNum(), get it with @ref AQFINTS_Element_GetMaxNum().</p>


@anchor AQFINTS_ELEMENT_minSize
<h2>minSize</h2>

<p>Set this property with @ref AQFINTS_Element_SetMinSize(), get it with @ref AQFINTS_Element_GetMinSize().</p>


@anchor AQFINTS_ELEMENT_maxSize
<h2>maxSize</h2>

<p>Set this property with @ref AQFINTS_Element_SetMaxSize(), get it with @ref AQFINTS_Element_GetMaxSize().</p>


@anchor AQFINTS_ELEMENT_trustLevel
<h2>trustLevel</h2>

<p>Set this property with @ref AQFINTS_Element_SetTrustLevel(), get it with @ref AQFINTS_Element_GetTrustLevel().</p>


@anchor AQFINTS_ELEMENT_data
<h2>data</h2>

<p>Set this property with @ref AQFINTS_Element_SetData(), get it with @ref AQFINTS_Element_GetData().</p>


@anchor AQFINTS_ELEMENT_runtimeFlags
<h2>runtimeFlags</h2>

<p>Set this property with @ref AQFINTS_Element_SetRuntimeFlags(), get it with @ref AQFINTS_Element_GetRuntimeFlags().</p>

*/

/* define AQFINTS_ELEMENT_RTFLAGS */


/* define AQFINTS_ELEMENT_FLAGS */
#define AQFINTS_ELEMENT_FLAGS_ISBIN 0x00000001
#define AQFINTS_ELEMENT_FLAGS_LEFTFILL 0x00000002
#define AQFINTS_ELEMENT_FLAGS_RIGHTFILL 0x00000004


/* needed system headers */
#include <gwenhywfar/types.h>
#include <gwenhywfar/tree2.h>
#include <gwenhywfar/db.h>

/* pre-headers */
#include <libaqfints/aqfints.h>
#include <gwenhywfar/bindata.h>

typedef struct AQFINTS_ELEMENT AQFINTS_ELEMENT;
GWEN_TREE2_FUNCTION_DEFS(AQFINTS_ELEMENT, AQFINTS_Element)



typedef enum {
  AQFINTS_ElementType_Unknown = -1,
  AQFINTS_ElementType_Root,
  AQFINTS_ElementType_Group,
  AQFINTS_ElementType_De,
  AQFINTS_ElementType_Deg
} AQFINTS_ELEMENT_TYPE;


typedef enum {
  AQFINTS_ElementDataType_Unknown = -1,
  AQFINTS_ElementDataType_Int,
  AQFINTS_ElementDataType_Char,
  AQFINTS_ElementDataType_Bin
} AQFINTS_ELEMENT_DATATYPE;


/* post-headers */


AQFINTS_ELEMENT_TYPE AQFINTS_ElementType_fromString(const char *p_s);

AQFINTS_ELEMENT_DATATYPE AQFINTS_ElementDataType_fromString(const char *p_s);

const char *AQFINTS_ElementType_toString(AQFINTS_ELEMENT_TYPE p_i);

const char *AQFINTS_ElementDataType_toString(AQFINTS_ELEMENT_DATATYPE p_i);

/** Constructor. */
AQFINTS_ELEMENT *AQFINTS_Element_new(void);

/** Destructor. */
void AQFINTS_Element_free(AQFINTS_ELEMENT *p_struct);

void AQFINTS_Element_Attach(AQFINTS_ELEMENT *p_struct);

AQFINTS_ELEMENT *AQFINTS_Element_dup(const AQFINTS_ELEMENT *p_src);

AQFINTS_ELEMENT *AQFINTS_Element_copy(AQFINTS_ELEMENT *p_struct, const AQFINTS_ELEMENT *p_src);

/** Getter.
 * Use this function to get the member "flags" (see @ref AQFINTS_ELEMENT_flags)
*/
uint32_t AQFINTS_Element_GetFlags(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "elementType" (see @ref AQFINTS_ELEMENT_elementType)
*/
AQFINTS_ELEMENT_TYPE AQFINTS_Element_GetElementType(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "id" (see @ref AQFINTS_ELEMENT_id)
*/
const char *AQFINTS_Element_GetId(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "name" (see @ref AQFINTS_ELEMENT_name)
*/
const char *AQFINTS_Element_GetName(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "version" (see @ref AQFINTS_ELEMENT_version)
*/
int AQFINTS_Element_GetVersion(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "type" (see @ref AQFINTS_ELEMENT_type)
*/
const char *AQFINTS_Element_GetType(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "minNum" (see @ref AQFINTS_ELEMENT_minNum)
*/
int AQFINTS_Element_GetMinNum(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "maxNum" (see @ref AQFINTS_ELEMENT_maxNum)
*/
int AQFINTS_Element_GetMaxNum(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "minSize" (see @ref AQFINTS_ELEMENT_minSize)
*/
int AQFINTS_Element_GetMinSize(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "maxSize" (see @ref AQFINTS_ELEMENT_maxSize)
*/
int AQFINTS_Element_GetMaxSize(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "trustLevel" (see @ref AQFINTS_ELEMENT_trustLevel)
*/
int AQFINTS_Element_GetTrustLevel(const AQFINTS_ELEMENT *p_struct);

/** Getter.
 * Use this function to get the member "runtimeFlags" (see @ref AQFINTS_ELEMENT_runtimeFlags)
*/
uint32_t AQFINTS_Element_GetRuntimeFlags(const AQFINTS_ELEMENT *p_struct);

/** Setter.
 * Use this function to set the member "flags" (see @ref AQFINTS_ELEMENT_flags)
*/
void AQFINTS_Element_SetFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);

/** Add flags.
 * Use this function to add flags to member "flags" (see @ref AQFINTS_ELEMENT_flags)
*/
void AQFINTS_Element_AddFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);
/** Add flags.
 * Use this function to add flags to member "flags" (see @ref AQFINTS_ELEMENT_flags)
*/
void AQFINTS_Element_SubFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);

/** Setter.
 * Use this function to set the member "elementType" (see @ref AQFINTS_ELEMENT_elementType)
*/
void AQFINTS_Element_SetElementType(AQFINTS_ELEMENT *p_struct, AQFINTS_ELEMENT_TYPE p_src);

/** Setter.
 * Use this function to set the member "id" (see @ref AQFINTS_ELEMENT_id)
*/
void AQFINTS_Element_SetId(AQFINTS_ELEMENT *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "name" (see @ref AQFINTS_ELEMENT_name)
*/
void AQFINTS_Element_SetName(AQFINTS_ELEMENT *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "version" (see @ref AQFINTS_ELEMENT_version)
*/
void AQFINTS_Element_SetVersion(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "type" (see @ref AQFINTS_ELEMENT_type)
*/
void AQFINTS_Element_SetType(AQFINTS_ELEMENT *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "minNum" (see @ref AQFINTS_ELEMENT_minNum)
*/
void AQFINTS_Element_SetMinNum(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "maxNum" (see @ref AQFINTS_ELEMENT_maxNum)
*/
void AQFINTS_Element_SetMaxNum(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "minSize" (see @ref AQFINTS_ELEMENT_minSize)
*/
void AQFINTS_Element_SetMinSize(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "maxSize" (see @ref AQFINTS_ELEMENT_maxSize)
*/
void AQFINTS_Element_SetMaxSize(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "trustLevel" (see @ref AQFINTS_ELEMENT_trustLevel)
*/
void AQFINTS_Element_SetTrustLevel(AQFINTS_ELEMENT *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "runtimeFlags" (see @ref AQFINTS_ELEMENT_runtimeFlags)
*/
void AQFINTS_Element_SetRuntimeFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);

/** Add flags.
 * Use this function to add flags to member "runtimeFlags" (see @ref AQFINTS_ELEMENT_runtimeFlags)
*/
void AQFINTS_Element_AddRuntimeFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);
/** Add flags.
 * Use this function to add flags to member "runtimeFlags" (see @ref AQFINTS_ELEMENT_runtimeFlags)
*/
void AQFINTS_Element_SubRuntimeFlags(AQFINTS_ELEMENT *p_struct, uint32_t p_src);

void AQFINTS_Element_ReadDb(AQFINTS_ELEMENT *p_struct, GWEN_DB_NODE *p_db);

int AQFINTS_Element_WriteDb(const AQFINTS_ELEMENT *p_struct, GWEN_DB_NODE *p_db);

AQFINTS_ELEMENT *AQFINTS_Element_fromDb(GWEN_DB_NODE *p_db);

int AQFINTS_Element_toDb(const AQFINTS_ELEMENT *p_struct, GWEN_DB_NODE *p_db);

 void AQFINTS_Element_SetData(AQFINTS_ELEMENT *st, uint8_t *ptr, uint32_t len);
 void AQFINTS_Element_SetDataCopy(AQFINTS_ELEMENT *st, const uint8_t *ptr, uint32_t len);
 void AQFINTS_Element_SetTextDataCopy(AQFINTS_ELEMENT *st, const char *ptr);
 const uint8_t* AQFINTS_Element_GetDataPointer(const AQFINTS_ELEMENT *st);
 uint32_t AQFINTS_Element_GetDataLength(const AQFINTS_ELEMENT *st);
 const char* AQFINTS_Element_GetDataAsChar(const AQFINTS_ELEMENT *st, const char *defaultValue);
 int AQFINTS_Element_GetDataAsInt(const AQFINTS_ELEMENT *st, int defaultValue);
 void AQFINTS_Element_SetDataAsInt(AQFINTS_ELEMENT *st, int value);
/* end-headers */


#ifdef __cplusplus
}
#endif

#endif

