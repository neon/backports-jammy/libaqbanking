/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "jobqueue.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AB_JOBQUEUE_JOBQUEUE_P_H
#define AB_JOBQUEUE_JOBQUEUE_P_H

#include "./jobqueue.h"


#ifdef __cplusplus
extern "C" {
#endif

struct AB_JOBQUEUE {
  GWEN_INHERIT_ELEMENT(AB_JOBQUEUE)
  GWEN_LIST_ELEMENT(AB_JOBQUEUE)
  int _refCount;
  int jobType; /* volatile */
  AB_TRANSACTION_LIST2 *transactionList; /* volatile */
};

#ifdef __cplusplus
}
#endif

#endif

