/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "swiftdescr.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "./swiftdescr_p.h"

#include <gwenhywfar/misc.h>
#include <gwenhywfar/debug.h>

/* code headers */

/* macro functions */
GWEN_LIST_FUNCTIONS(AB_SWIFT_DESCR, AB_SwiftDescr)


AB_SWIFT_DESCR *AB_SwiftDescr_new(void) {
  AB_SWIFT_DESCR *p_struct;

  GWEN_NEW_OBJECT(AB_SWIFT_DESCR, p_struct)
  p_struct->_refCount=1;
  GWEN_LIST_INIT(AB_SWIFT_DESCR, p_struct)
  /* members */
  p_struct->family=NULL;
  p_struct->version1=0;
  p_struct->version2=0;
  p_struct->version3=0;
  p_struct->alias1=NULL;
  p_struct->alias2=NULL;
  /* virtual functions */

  return p_struct;
}

void AB_SwiftDescr_free(AB_SWIFT_DESCR *p_struct) {
  if (p_struct) {
  assert(p_struct->_refCount);
  if (p_struct->_refCount==1) {
    GWEN_LIST_FINI(AB_SWIFT_DESCR, p_struct)
  /* members */
    free(p_struct->family); p_struct->family=NULL;
    free(p_struct->alias1); p_struct->alias1=NULL;
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->_refCount=0;
    GWEN_FREE_OBJECT(p_struct);
  }
  else
    p_struct->_refCount--;
  }
}

void AB_SwiftDescr_Attach(AB_SWIFT_DESCR *p_struct) {
  assert(p_struct);
  assert(p_struct->_refCount);
  p_struct->_refCount++;
}

AB_SWIFT_DESCR *AB_SwiftDescr_dup(const AB_SWIFT_DESCR *p_src) {
  AB_SWIFT_DESCR *p_struct;

  assert(p_src);
  p_struct=AB_SwiftDescr_new();
  /* member "family" */
  if (p_struct->family) {
    free(p_struct->family); p_struct->family=NULL;
    p_struct->family=NULL;
  }
  if (p_src->family) {
    p_struct->family=strdup(p_src->family);
  }


  /* member "version1" */
    p_struct->version1=p_src->version1;


  /* member "version2" */
    p_struct->version2=p_src->version2;


  /* member "version3" */
    p_struct->version3=p_src->version3;


  /* member "alias1" */
  if (p_struct->alias1) {
    free(p_struct->alias1); p_struct->alias1=NULL;
    p_struct->alias1=NULL;
  }
  if (p_src->alias1) {
    p_struct->alias1=strdup(p_src->alias1);
  }


  /* member "alias2" */
  if (p_struct->alias2) {
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->alias2=NULL;
  }
  if (p_src->alias2) {
    p_struct->alias2=strdup(p_src->alias2);
  }


  return p_struct;
}

AB_SWIFT_DESCR *AB_SwiftDescr_copy(AB_SWIFT_DESCR *p_struct, const AB_SWIFT_DESCR *p_src) {
  assert(p_struct);
  assert(p_src);
  /* member "family" */
  if (p_struct->family) {
    free(p_struct->family); p_struct->family=NULL;
    p_struct->family=NULL;
  }
  if (p_src->family) {
    p_struct->family=strdup(p_src->family);
  }


  /* member "version1" */
    p_struct->version1=p_src->version1;


  /* member "version2" */
    p_struct->version2=p_src->version2;


  /* member "version3" */
    p_struct->version3=p_src->version3;


  /* member "alias1" */
  if (p_struct->alias1) {
    free(p_struct->alias1); p_struct->alias1=NULL;
    p_struct->alias1=NULL;
  }
  if (p_src->alias1) {
    p_struct->alias1=strdup(p_src->alias1);
  }


  /* member "alias2" */
  if (p_struct->alias2) {
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->alias2=NULL;
  }
  if (p_src->alias2) {
    p_struct->alias2=strdup(p_src->alias2);
  }


  return p_struct;
}

const char *AB_SwiftDescr_GetFamily(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->family;
}

int AB_SwiftDescr_GetVersion1(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->version1;
}

int AB_SwiftDescr_GetVersion2(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->version2;
}

int AB_SwiftDescr_GetVersion3(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->version3;
}

const char *AB_SwiftDescr_GetAlias1(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->alias1;
}

const char *AB_SwiftDescr_GetAlias2(const AB_SWIFT_DESCR *p_struct){
  assert(p_struct);
  return p_struct->alias2;
}

void AB_SwiftDescr_SetFamily(AB_SWIFT_DESCR *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->family) {
    free(p_struct->family); p_struct->family=NULL;
    p_struct->family=NULL;
  }
  if (p_src) {
    p_struct->family=strdup(p_src);
  }
  else {
    p_struct->family=NULL;
  }

}

void AB_SwiftDescr_SetVersion1(AB_SWIFT_DESCR *p_struct, int p_src){
  assert(p_struct);
  p_struct->version1=p_src;
}

void AB_SwiftDescr_SetVersion2(AB_SWIFT_DESCR *p_struct, int p_src){
  assert(p_struct);
  p_struct->version2=p_src;
}

void AB_SwiftDescr_SetVersion3(AB_SWIFT_DESCR *p_struct, int p_src){
  assert(p_struct);
  p_struct->version3=p_src;
}

void AB_SwiftDescr_SetAlias1(AB_SWIFT_DESCR *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->alias1) {
    free(p_struct->alias1); p_struct->alias1=NULL;
    p_struct->alias1=NULL;
  }
  if (p_src) {
    p_struct->alias1=strdup(p_src);
  }
  else {
    p_struct->alias1=NULL;
  }

}

void AB_SwiftDescr_SetAlias2(AB_SWIFT_DESCR *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->alias2) {
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->alias2=NULL;
  }
  if (p_src) {
    p_struct->alias2=strdup(p_src);
  }
  else {
    p_struct->alias2=NULL;
  }

}

AB_SWIFT_DESCR_LIST *AB_SwiftDescr_List_dup(const AB_SWIFT_DESCR_LIST *p_src) {
  AB_SWIFT_DESCR_LIST *p_dest;
  AB_SWIFT_DESCR *p_elem;

  assert(p_src);
  p_dest=AB_SwiftDescr_List_new();
  p_elem=AB_SwiftDescr_List_First(p_src);
  while(p_elem) {
    AB_SWIFT_DESCR *p_cpy;

    p_cpy=AB_SwiftDescr_dup(p_elem);
    AB_SwiftDescr_List_Add(p_cpy, p_dest);
    p_elem=AB_SwiftDescr_List_Next(p_elem);
  }

  return p_dest;
}

void AB_SwiftDescr_ReadDb(AB_SWIFT_DESCR *p_struct, GWEN_DB_NODE *p_db){
  assert(p_struct);
  /* member "family" */
  if (p_struct->family) {
    free(p_struct->family); p_struct->family=NULL;
    p_struct->family=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "family", 0, NULL); if (s) p_struct->family=strdup(s); }
  /* member "version1" */
  p_struct->version1=GWEN_DB_GetIntValue(p_db, "version1", 0, 0);
  /* member "version2" */
  p_struct->version2=GWEN_DB_GetIntValue(p_db, "version2", 0, 0);
  /* member "version3" */
  p_struct->version3=GWEN_DB_GetIntValue(p_db, "version3", 0, 0);
  /* member "alias1" */
  if (p_struct->alias1) {
    free(p_struct->alias1); p_struct->alias1=NULL;
    p_struct->alias1=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "alias1", 0, NULL); if (s) p_struct->alias1=strdup(s); }
  /* member "alias2" */
  if (p_struct->alias2) {
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->alias2=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "alias2", 0, NULL); if (s) p_struct->alias2=strdup(s); }
}

int AB_SwiftDescr_WriteDb(const AB_SWIFT_DESCR *p_struct, GWEN_DB_NODE *p_db) {
  int p_rv;

  assert(p_struct);
  /* member "family" */
  if (p_struct->family) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "family", p_struct->family); else { GWEN_DB_DeleteVar(p_db, "family"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "version1" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "version1", p_struct->version1);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "version2" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "version2", p_struct->version2);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "version3" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "version3", p_struct->version3);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "alias1" */
  if (p_struct->alias1) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "alias1", p_struct->alias1); else { GWEN_DB_DeleteVar(p_db, "alias1"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "alias2" */
  if (p_struct->alias2) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "alias2", p_struct->alias2); else { GWEN_DB_DeleteVar(p_db, "alias2"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  return 0;
}

AB_SWIFT_DESCR *AB_SwiftDescr_fromDb(GWEN_DB_NODE *p_db) {
  AB_SWIFT_DESCR *p_struct;
  p_struct=AB_SwiftDescr_new();
  AB_SwiftDescr_ReadDb(p_struct, p_db);
  return p_struct;
}

int AB_SwiftDescr_toDb(const AB_SWIFT_DESCR *p_struct, GWEN_DB_NODE *p_db) {
  return AB_SwiftDescr_WriteDb(p_struct, p_db);
}

void AB_SwiftDescr_ReadXml(AB_SWIFT_DESCR *p_struct, GWEN_XMLNODE *p_db){
  assert(p_struct);
  /* member "family" */
  if (p_struct->family) {
    free(p_struct->family); p_struct->family=NULL;
    p_struct->family=NULL;
  }
  { const char *s; s=GWEN_XMLNode_GetCharValue(p_db, "family", NULL); if (s) p_struct->family=strdup(s); }
  /* member "version1" */
  p_struct->version1=GWEN_XMLNode_GetIntValue(p_db, "version1", 0);
  /* member "version2" */
  p_struct->version2=GWEN_XMLNode_GetIntValue(p_db, "version2", 0);
  /* member "version3" */
  p_struct->version3=GWEN_XMLNode_GetIntValue(p_db, "version3", 0);
  /* member "alias1" */
  if (p_struct->alias1) {
    free(p_struct->alias1); p_struct->alias1=NULL;
    p_struct->alias1=NULL;
  }
  { const char *s; s=GWEN_XMLNode_GetCharValue(p_db, "alias1", NULL); if (s) p_struct->alias1=strdup(s); }
  /* member "alias2" */
  if (p_struct->alias2) {
    free(p_struct->alias2); p_struct->alias2=NULL;
    p_struct->alias2=NULL;
  }
  { const char *s; s=GWEN_XMLNode_GetCharValue(p_db, "alias2", NULL); if (s) p_struct->alias2=strdup(s); }
}

void AB_SwiftDescr_WriteXml(const AB_SWIFT_DESCR *p_struct, GWEN_XMLNODE *p_db) {
  assert(p_struct);
  /* member "family" */
  GWEN_XMLNode_SetCharValue(p_db, "family", p_struct->family);


  /* member "version1" */
  GWEN_XMLNode_SetIntValue(p_db, "version1", p_struct->version1);


  /* member "version2" */
  GWEN_XMLNode_SetIntValue(p_db, "version2", p_struct->version2);


  /* member "version3" */
  GWEN_XMLNode_SetIntValue(p_db, "version3", p_struct->version3);


  /* member "alias1" */
  GWEN_XMLNode_SetCharValue(p_db, "alias1", p_struct->alias1);


  /* member "alias2" */
  GWEN_XMLNode_SetCharValue(p_db, "alias2", p_struct->alias2);


}

void AB_SwiftDescr_toXml(const AB_SWIFT_DESCR *p_struct, GWEN_XMLNODE *p_db) {
  AB_SwiftDescr_WriteXml(p_struct, p_db);
}

AB_SWIFT_DESCR *AB_SwiftDescr_fromXml(GWEN_XMLNODE *p_db) {
  AB_SWIFT_DESCR *p_struct;
  p_struct=AB_SwiftDescr_new();
  AB_SwiftDescr_ReadXml(p_struct, p_db);
  return p_struct;
}

int AB_SwiftDescr_Matches(const AB_SWIFT_DESCR *d, const char *wantedFamily, int wantedVersion1, int wantedVersion2, int wantedVersion3) { const char *family; int version1; int version2; int version3; if (!wantedFamily) wantedFamily="*"; family=AB_SwiftDescr_GetFamily(d); version1=AB_SwiftDescr_GetVersion1(d); version2=AB_SwiftDescr_GetVersion2(d); version3=AB_SwiftDescr_GetVersion3(d); if (family==NULL) family=""; if ((-1!=GWEN_Text_ComparePattern(family, wantedFamily, 0)) && (wantedVersion1==0 || version1==wantedVersion1) && (wantedVersion2==0 || version2==wantedVersion2) && (wantedVersion3==0 || version3==wantedVersion3)) return 1; return 0; }
AB_SWIFT_DESCR *AB_SwiftDescr_List__FindInternal(AB_SWIFT_DESCR *d, const char *wantedFamily, int wantedVersion1, int wantedVersion2, int wantedVersion3){ if (!wantedFamily) wantedFamily="*"; while(d) { if (1==AB_SwiftDescr_Matches(d, wantedFamily, wantedVersion1, wantedVersion2, wantedVersion3)) break; d=AB_SwiftDescr_List_Next(d); } /* while */ return d; }
AB_SWIFT_DESCR *AB_SwiftDescr_List_FindFirst(const AB_SWIFT_DESCR_LIST *dl, const char *wantedFamily, int wantedVersion1, int wantedVersion2, int wantedVersion3) { AB_SWIFT_DESCR *d; if (AB_SwiftDescr_List_GetCount(dl)==0) { DBG_INFO(AQBANKING_LOGDOMAIN, "empty list"); return NULL; } d=AB_SwiftDescr_List_First(dl); assert(d); return AB_SwiftDescr_List__FindInternal(d, wantedFamily, wantedVersion1, wantedVersion2, wantedVersion3); }
AB_SWIFT_DESCR *AB_SwiftDescr_List_FindNext(AB_SWIFT_DESCR *d, const char *wantedFamily, int wantedVersion1, int wantedVersion2, int wantedVersion3){ assert(d); d=AB_SwiftDescr_List_Next(d); if (d==NULL) { DBG_INFO(AQBANKING_LOGDOMAIN, "No more entries in list"); return NULL; } return AB_SwiftDescr_List__FindInternal(d, wantedFamily, wantedVersion1, wantedVersion2, wantedVersion3); }
AB_SWIFT_DESCR *AB_SwiftDescr_FromString(const char *inputName) { GWEN_STRINGLIST *slist; int count; /* add delimiters here if needed */ slist=GWEN_StringList_fromString(inputName, ":._- \t", 0); if (slist==NULL) { DBG_ERROR(AQBANKING_LOGDOMAIN, "Could not parse string [%s] into list", inputName); return NULL; } count=GWEN_StringList_Count(slist); if (count>2) { int i; for (i=count-1; i>=0; i--) { const char *s; s=GWEN_StringList_StringAt(slist, i); DBG_DEBUG(AQBANKING_LOGDOMAIN, "Handling string[%d of %d]: \"%s\"", i, count, s?s:"<empty>"); if (s && (strcasecmp(s, "camt")==0 || strcasecmp(s, "pain")==0)) { if ((count-i)<4) { DBG_INFO(AQBANKING_LOGDOMAIN, "Too few entries left in string list (source: [%s])", inputName); break; } else { const char *family; int version1; int version2; int version3; AB_SWIFT_DESCR *d; family=s; i++; s=GWEN_StringList_StringAt(slist, i); if (!(s && *s && 1==sscanf(s, "%d", &version1))) { DBG_ERROR(AQBANKING_LOGDOMAIN, "No valid string for version1 [%s] ", s?s:"<empty>"); GWEN_StringList_free(slist); return NULL; } i++; s=GWEN_StringList_StringAt(slist, i); if (!(s && *s && 1==sscanf(s, "%d", &version2))) { DBG_ERROR(AQBANKING_LOGDOMAIN, "No valid string for version2 [%s] ", s?s:"<empty>"); GWEN_StringList_free(slist); return NULL; } i++; s=GWEN_StringList_StringAt(slist, i); if (!(s && *s && 1==sscanf(s, "%d", &version3))) { DBG_ERROR(AQBANKING_LOGDOMAIN, "No valid string for version3 [%s] ", s?s:"<empty>"); GWEN_StringList_free(slist); return NULL; } i++; DBG_INFO(AQBANKING_LOGDOMAIN, "Creating descriptor %s.%03d.%03d.%02d", family?family:"<empty>", version1, version2, version3); d=AB_SwiftDescr_new(); AB_SwiftDescr_SetFamily(d, family); AB_SwiftDescr_SetVersion1(d, version1); AB_SwiftDescr_SetVersion2(d, version2); AB_SwiftDescr_SetVersion3(d, version3); GWEN_StringList_free(slist); return d; } } /* if camt or pain */ } /* for */ } /* if enough entries in string list to be a valid descriptor */ else { DBG_INFO(AQBANKING_LOGDOMAIN, "Too few entries in string list (source: [%s])", inputName); } GWEN_StringList_free(slist); return NULL; }

/* code headers */

