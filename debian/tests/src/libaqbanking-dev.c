#include <stdio.h>

#include <aqbanking/banking.h>
#include <gwenhywfar/cgui.h>

int main(int argc, const char *argv[]) {
  int major, minor, patchlevel, build;
  AB_BANKING *banking;
  GWEN_GUI *gui;

  gui = GWEN_Gui_CGui_new();
  GWEN_Gui_SetGui(gui);

  banking = AB_Banking_new(__FILE__, NULL, 0);
  AB_Banking_Init(banking);

  AB_Banking_GetVersion(&major, &minor, &patchlevel, &build);

  AB_Banking_Fini(banking);
  AB_Banking_free(banking);

  GWEN_Gui_free(gui);
  
  printf("Successfully tested AqBanking %d.%d.%d.%d\n",
         major, minor, patchlevel, build);

  return 0;
}
